const DEFAULT_COVER = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAAEsBAMAAABu6AEDAAAAG1BMVEXMzMyWlpacnJyqqqrFxcWxsbGjo6O3t7e+vr6He3KoAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAC30lEQVR4nO3bvW/aQBgG8MfGGI8+E0jG46MJo0kbqaMpEV2BIbONqMhoqERXaKX+3b33MIIoIUIFVFV6fhLvYR/4OfvO3gwQERERERERERERERERERERERERERERERERERHRhZU0vPtWAozryd7u39En09eMi3KSnoYbNVKUo0zv9gZqohJ0m1Vsyim+NjRGcfkGji5f73b7bbPXqyNLbDkpoqG0HKKKee592+12huilJnM+tOWkiB9zjQi4RXcz1HlsB91fmQj/altOioBjI5rIBjLn8K+CzaX3stwNTa8tJ0eYC67QzJQcKaibYUuCqsE1K0HbcnLEaDpQUNNBTba73dxGNKLEdLmhLSdHuEpNoGTtGL2o6OjGZzwL7/751swGGrLt17Yd6RnnwqhiInNurM0tBywSuOdcURMENxgVF6o5l+fFOjdncab7QiKylR9iHZfk7q7U7Ipy2+gOz3R3S8S6muXw5UFljq0DOZlKNIlwpmeURATZnfnSbcnmKEd3ZdpFJA9ZtS1ERERERERERERERERERERERERERERERBfhFJ//IULJW03HRIRmy1FKycsybojNuyT9xgwYqCkcXbyx8YbQ+3h8hK31FZZFhPch+WXK4zhxnlHJDkXgC+5bcJOK+VJuxnDGoTSl5kgixi34uenZi7hOMSsiSsNNKcfOEv78cER5OhiW8h6+4+nxAc7UNk+PMijHdAUaD/sRYSfQRYSbbIqnHXf189DEmQvlJp4O4nH+GW0zXiexTRs9iTBdmJmPmTSZBxux9PMiwtmW0Kmks0MRZrrt4HQn1XIY+Yc04XYuQvws5S/Owl3g9VngTh88C/mNDDV9Ts3Q7XGk2TsLd4wXEeUaXs8FRsN3ImQuMM4XQyyTvhxHmt1coHL9MsJu2Lq3onD4JpKfmhWFddJLUGl05IfS7FYUKvqNiM3F7KvtffFOxBH8/O//e6TFxROczsUjiP6tP93DbOAoElQuAAAAAElFTkSuQmCC'

/**
 * @param {string} uuid
 * @param {string} data
 */
export function coverAdd (uuid, data) {
  localStorage.setItem(`cover_${uuid}`, data)
}

export function coverDelete (uuid) {
  localStorage.removeItem(`cover_${uuid}`)
}

/**
 * @param {string} uuid
 * @returns {string}
 */
export function coverGet (uuid) {
  const cover = localStorage.getItem(`cover_${uuid}`)
  return cover ?? DEFAULT_COVER
}
