const BASE_URL = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'

export function searchByIsbn (isbn) {
  return fetch(BASE_URL + isbn)
    .then(response => {
      if (response.ok) {
        return response.json()
      }
      throw new Error(response.statusText)
    })
    .then(payload => payload.totalItems > 0 ? payload.items[0].volumeInfo : null)
}
