import Dexie from 'dexie'
import { v4 as uuidv4 } from 'uuid'

const database = new Dexie('books')
database
  .version(4)
  .stores({
    books: 'uuid,isbn,name'
  })

/**
 * @param {string|null} abstract
 * @param {string} author
 * @param {string} isbn
 * @param {string} name
 * @returns {Promise}
 */
export function bookAdd ({ abstract, author, isbn, name }) {
  return database.books.add({
    abstract,
    author,
    isbn,
    name,
    uuid: uuidv4()
  })
}

/**
 * @returns {Promise}
 */
export function bookGetAll () {
  return database.books.orderBy('name').toArray()
}

/**
 * @param {string} uuid
 * @returns {Promise}
 */
export function bookGetByUuid (uuid) {
  return database.books.get(uuid)
}

/**
 * @param {string} uuid
 * @returns {Promise}
 */
export function bookDelete (uuid) {
  return database.books.delete(uuid)
}

/**
 * @param {string|null} abstract
 * @param {string} author
 * @param {string} isbn
 * @param {string} name
 * @returns {Promise}
 */
export function bookUpdate (isbn, { abstract, author, name }) {
  return database.books.update(isbn, { abstract, author, name })
}
