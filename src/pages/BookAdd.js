import React from 'react'
import { useHistory } from 'react-router-dom'
import { bookAdd } from '../service/storage-book'
import { coverAdd } from '../service/storage-cover'
import BookForm from '../component/BookForm'
import PageTitle from '../component/PageTitle'

export default function BookAdd () {
  const history = useHistory()

  function handleSave (data) {
    bookAdd(data)
      .then(uuid => {
        if (data.cover) {
          coverAdd(uuid, data.cover)
        }
      })
      .then(() => history.push('/'))
  }

  return (
    <div>
      <PageTitle title="Add a book">
        <button type="button" className="btn btn-primary" onClick={() => { location.href = '/' }}>
          Index of books
        </button>
      </PageTitle>
      <div className="mt-4">
        <BookForm onSave={handleSave}/>
      </div>
    </div>
  )
}
