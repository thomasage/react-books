import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useLiveQuery } from 'dexie-react-hooks'
import { bookGetAll } from '../service/storage-book'
import BookList from '../component/BookList'
import PageTitle from '../component/PageTitle'

export default function Home () {
  const [books, setBooks] = useState()

  useLiveQuery(() => {
    bookGetAll()
      .then(books => setBooks(books))
  }, [])

  if (!books) {
    return null
  }

  return (
    <div>
      <PageTitle title="All the books">
        <Link to="/add" className="btn btn-primary">
          Add a book
        </Link>
      </PageTitle>
      <BookList books={books}/>
    </div>
  )
}
