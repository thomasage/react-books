import React, { useEffect, useState } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import BookForm from '../component/BookForm'
import PageTitle from '../component/PageTitle'
import { bookDelete, bookGetByUuid, bookUpdate } from '../service/storage-book'
import { coverAdd, coverDelete, coverGet } from '../service/storage-cover'

export default function BookEdit () {
  const history = useHistory()
  const { uuid } = useParams()
  const [book, setBook] = useState()
  const [cover, setCover] = useState()

  useEffect(() => {
    bookGetByUuid(uuid)
      .then(book => setBook(book))
      .then(() => setCover(coverGet(uuid)))
  }, [])

  if (!book) {
    return null
  }

  function handleDelete () {
    if (!confirm('Delete this book?')) {
      return
    }
    bookDelete(uuid)
      .then(() => { coverDelete(uuid) })
      .then(() => history.push('/'))
  }

  function handleSave (data) {
    bookUpdate(uuid, {
      abstract: data.abstract,
      author: data.author,
      name: data.name
    })
      .then(() => { if (data.cover) { coverAdd(uuid, data.cover) } })
      .then(() => history.push('/'))
  }

  return (
    <div>
      <PageTitle title={book.name}>
        <Link to="/" className="btn btn-primary">
          Index of books
        </Link>
      </PageTitle>
      <div className="mt-4 d-flex flex-row">
        <div className="flex-grow-1">
          <BookForm
            defaultAbstract={book.abstract}
            defaultAuthor={book.author}
            defaultIsbn={book.isbn}
            defaultName={book.name}
            onSave={handleSave}
          />
        </div>
        <div className="ms-5">
          {cover && (
            <img
              src={cover}
              alt="Cover"
              style={{ height: '300px', width: '100%' }}
            />
          )}
          <div className="mt-5">
            <button type="button" className="btn btn-outline-danger" onClick={handleDelete}>
              Delete this book
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
