import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './pages/Home'
import BookAdd from './pages/BookAdd'
import BookEdit from './pages/BookEdit'

function App () {
  return (
    <Router>
      <div className="container">
        <Switch>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="/add" exact>
            <BookAdd/>
          </Route>
          <Route path="/edit/:uuid" exact>
            <BookEdit/>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
