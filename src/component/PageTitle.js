import React from 'react'
import PropTypes from 'prop-types'

export default function PageTitle ({ children, title }) {
  return (
    <div className="mt-4 d-flex flex-row align-items-center">
      <h2 className="flex-grow-1 m-0">{title}</h2>
      <div>
        {children}
      </div>
    </div>
  )
}

PageTitle.propTypes = {
  children: PropTypes.object,
  title: PropTypes.string.isRequired
}
