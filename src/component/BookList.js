import React from 'react'
import PropTypes from 'prop-types'
import { coverGet } from '../service/storage-cover'
import BookListEntry from './BookListEntry'

export default function BookList ({ books }) {
  function getCover (uuid) {
    return coverGet(uuid)
  }

  return (
    <div className="row">
      {books.map(book => (
        <div className="col-12 col-lg-6 col-xl-4 g-4" key={book.uuid}>
          <BookListEntry
            abstract={book.abstract}
            author={book.author}
            cover={getCover(book.uuid)}
            name={book.name}
            uuid={book.uuid}
          />
        </div>
      ))}
    </div>
  )
}

BookList.propTypes = {
  books: PropTypes.arrayOf(PropTypes.shape({
    author: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired
  })).isRequired
}
