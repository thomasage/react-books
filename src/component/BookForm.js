import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { searchByIsbn } from '../service/api-google-books'

export default function BookForm ({ defaultAbstract, defaultAuthor, defaultIsbn, defaultName, onSave }) {
  const [formData, setFormData] = useState({
    abstract: defaultAbstract,
    author: defaultAuthor,
    cover: null,
    isbn: defaultIsbn,
    name: defaultName
  })
  const [searching, setSearching] = useState(false)
  const [coverHelp, setCoverHelp] = useState(null)

  const sampleIsbns = [
    '9781781100219',
    '9781781100226',
    '9781408855676',
    '9781408865422',
    '9781781100530',
    '9781781100547',
    '9781781100264'
  ]

  function convertImageToDataUrl (file, callback) {
    const reader = new FileReader()
    reader.onload = event => {
      callback(event.target.result)
    }
    reader.readAsDataURL(file)
  }

  function handleSearchByIsbn () {
    if (formData.isbn.length !== 13) {
      return
    }
    setSearching(true)
    searchByIsbn(formData.isbn)
      .then(result => {
        if (result) {
          setFormData({
            ...formData,
            abstract: result.description,
            author: result.authors.join(', '),
            name: result.title
          })
          setCoverHelp(result.imageLinks.thumbnail)
        }
      })
      .finally(() => setSearching(false))
  }

  function handleSubmit (event) {
    event.preventDefault()
    onSave(formData)
  }

  function handleUpload (event) {
    const files = event.target.files
    if (files.length === 0) {
      return
    }
    convertImageToDataUrl(files[0], cover => {
      setFormData({ ...formData, cover })
    })
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="isbn" className="form-label">ISBN</label>
        <div className="d-flex flex-row align-items-start">
          <div className="flex-grow-1">
            <input
              type="text"
              id="isbn"
              required
              autoFocus
              className="form-control"
              value={formData.isbn}
              onChange={event => setFormData({ ...formData, isbn: event.target.value })}
            />
            <div className="form-text">
              Try with:
              {sampleIsbns.map(isbn => (
                <a href="#"
                   key={isbn}
                   className="ms-1"
                   onClick={event => setFormData({ ...formData, isbn })}>{isbn}</a>
              ))}
            </div>
          </div>
          {searching && (
            <div className="spinner-border ms-3" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          )}
          {searching || (
            <button type="button" className="btn btn-outline-info ms-3" onClick={handleSearchByIsbn}>
              Google
            </button>
          )}
        </div>
      </div>
      <div className="mt-4">
        <label htmlFor="name" className="form-label">Name</label>
        <input
          type="text"
          id="name"
          required
          className="form-control"
          value={formData.name}
          onChange={event => setFormData({ ...formData, name: event.target.value })}
        />
      </div>
      <div className="mt-4">
        <label htmlFor="author" className="form-label">Author</label>
        <input
          type="text"
          id="author"
          required
          className="form-control"
          value={formData.author}
          onChange={event => setFormData({ ...formData, author: event.target.value })}
        />
      </div>
      <div className="mt-4">
        <label htmlFor="abstract" className="form-label">Abstract</label>
        <textarea
          id="abstract"
          rows="10"
          className="form-control"
          value={formData.abstract}
          onChange={event => setFormData({ ...formData, abstract: event.target.value })}
        />
      </div>
      <div className="mt-4">
        <label htmlFor="cover" className="form-label">Cover</label>
        <input
          type="file"
          id="cover"
          accept="image/*"
          className="form-control"
          onChange={handleUpload}
        />
        {coverHelp && (
          <div className="form-text">
            Try <a href={coverHelp} target="_blank" rel="noreferrer">{coverHelp}</a>
          </div>
        )}
      </div>
      <div className="mt-4">
        <button type="submit" className="btn btn-success">
          Save
        </button>
      </div>
    </form>
  )
}

BookForm.defaultProps = {
  defaultAbstract: '',
  defaultAuthor: '',
  defaultIsbn: '',
  defaultName: ''
}
BookForm.propTypes = {
  defaultAbstract: PropTypes.string,
  defaultAuthor: PropTypes.string,
  defaultIsbn: PropTypes.string,
  defaultName: PropTypes.string,
  onSave: PropTypes.func.isRequired
}
