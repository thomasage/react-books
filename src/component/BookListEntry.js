import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default function BookListEntry ({ abstract, author, cover, name, uuid }) {
  return (
    <Link to={`/edit/${uuid}`}
          className="d-flex flex-row align-items-start text-dark text-decoration-none"
          title={abstract}>
      <img
        src={cover}
        alt="Cover"
        className="me-3"
        style={{ width: '150px' }}
      />
      <div className="flex-grow-1">
        <div className="mt-1 fw-bold">
          {name}
          <small className="float-end text-muted">{author}</small>
        </div>
        <div className="overflow-ellipsis">
          <small className="text-muted">{abstract}</small>
        </div>
      </div>
    </Link>
  )
}

BookListEntry.propTypes = {
  abstract: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  cover: PropTypes.string,
  name: PropTypes.string.isRequired,
  uuid: PropTypes.string.isRequired
}
